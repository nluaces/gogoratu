package io.nluaces.midori

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class MidoriBoxApplication

fun main(args: Array<String>) {
	runApplication<MidoriBoxApplication>(*args)
}
